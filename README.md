# Without address I was able to connect with "curl" inside the container
#  but not from the host browser.
kubectl proxy --address="0.0.0.0"

# Access through:
# http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login
