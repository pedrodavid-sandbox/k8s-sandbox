provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  name_regex = "amzn2-ami-hvm*"

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    # Can't run Gitlab Apps (Docker images not ready for ARM)
    # values = ["arm64"]
    values = ["x86_64"]
  }

  owners = ["amazon"]
}

resource "tls_private_key" "deployer" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = tls_private_key.deployer.public_key_openssh
}

resource "aws_security_group" "ssh-access" {
  name = "allow-ssh-access"

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]

    from_port = 22
    to_port = 22
    protocol = "tcp"
  }
}

resource "aws_security_group" "all-egress" {
  name = "allow-all-egress-trafic"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "http-access" {
  name = "allow-http-access"

  ingress {
    description = "Expose HTTP to internet"
    cidr_blocks = [ "0.0.0.0/0" ]

    ipv6_cidr_blocks = [ "::/0" ]

    from_port = 80
    to_port = 80
    protocol = "tcp"
  }

  ingress {
    description = "Expose TLS to internet"
    cidr_blocks = [ "0.0.0.0/0" ]

    ipv6_cidr_blocks = [ "::/0" ]

    from_port = 443
    to_port = 443
    protocol = "tcp"
  }

  ingress {
    description = "Expose Kubernetes to internet"
    cidr_blocks = [ "0.0.0.0/0" ]

    ipv6_cidr_blocks = [ "::/0" ]

    from_port = 6443
    to_port = 6443
    protocol = "tcp"
  }
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.amazon-linux-2.id

  # ARM based, can't use Gitlab Apps
  # instance_type = "t4g.small"

  # T3 and T3A use Intel Skylake and AMD EPYC 7000 respectively
  # The latter is cheaper
  instance_type = "t3a.small"
  key_name      = aws_key_pair.deployer.key_name

  vpc_security_group_ids = [
    aws_security_group.ssh-access.id,
    aws_security_group.http-access.id,
    aws_security_group.all-egress.id
  ]

  tags = {
    Name = "HelloWorld"
  }

  # provisioner "local-exec" {
  #  command = "(ssh-keygen -F ${aws_instance.web.public_ip} || ssh-keyscan -H -t ecdsa ${aws_instance.web.public_ip} >> ~/.ssh/known_hosts) >/dev/null 2>&1"
  # }
}

resource "local_file" "key" {
  filename           = pathexpand("~/.ssh/aws_id_rsa")
  sensitive_content  = tls_private_key.deployer.private_key_pem

  # Avoid the following error by restriting file permissions to the user (rw).
  #
  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  # @         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  # Permissions 0755 for '/home/terraform/.ssh/id_rsa' are too open.
  # It is required that your private key files are NOT accessible by others.
  # This private key will be ignored.
  # Load key "/home/terraform/.ssh/id_rsa": bad permissions
  file_permission    = 600
}

resource "local_file" "ssh_config" {
  filename           = pathexpand("~/.ssh/config")
  content            = <<EOT
Host dev
    HostName ${aws_instance.web.public_ip}
    User ec2-user
    IdentityFile ~/.ssh/aws_id_rsa
EOT
}

output "instance_ip_addr" {
  value = aws_instance.web.public_ip
}
