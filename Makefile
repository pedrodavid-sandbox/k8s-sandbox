.PHONY: install
install:
	docker build -t dev/base-image:latest dev/base-image
	docker build -t dev/terraform:latest  dev/terraform
	docker build -t dev/kubectl:latest    dev/kubectl

.PHONY: run
run:
	docker run -it --rm dev/base-image:latest bash

.PHONY: terraform-shell
terraform-shell:
	docker run --rm -v $$HOME/.aws/credentials:/home/terraform/.aws/credentials:ro \
		-it --entrypoint sh dev/terraform:latest

.PHONY: kubectl-shell
kubectl-shell:
	docker run --rm -it -p 127.0.0.1:8001:8001/tcp dev/kubectl:latest bash
	
# "trap 'terraform destroy -auto-approve' EXIT; sh"
